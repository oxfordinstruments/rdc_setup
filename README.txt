How to setup sd card for RDC
1. Flash rdc-sd-2.1.img to sd card
2. Copy files from boot folder to boot partition and eject the disk
3. Boot the rpi from the sd card and login as pi
4. sudo dpkg -i /boot/rdcsetup_*.deb
5. sudo rdcsetup
6. sudo halt
8. Insert SD into linux computer
7. sudo fdisk /dev/mmcblk0
	1. Note the 2nd partition end number (5765168)
8. Make a copy of the sd card
	1. dd if=/dev/sdX of=name.img status=progress bs=512 count=[step 7 number]
