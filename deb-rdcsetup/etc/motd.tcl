#!/usr/bin/env tclsh
# MOTD script original? / mod mewbies.com

# * Variables
set var(user) $env(USER)
set var(path) $env(PWD)

# * Calculate last login
set lastlog [exec -- lastlog -u $var(user)]
set ll(1)  [lindex $lastlog 7]
set ll(2)  [lindex $lastlog 8]
set ll(3)  [lindex $lastlog 9]
set ll(4)  [lindex $lastlog 10]
set ll(5)  [lindex $lastlog 6]

# * Calculate current system uptime
set uptime    [exec -- /usr/bin/cut -d. -f1 /proc/uptime]
set up(days)  [expr {$uptime/60/60/24}]
set up(hours) [expr {$uptime/60/60%24}]
set up(mins)  [expr {$uptime/60%60}]
set up(secs)  [expr {$uptime%60}]

# * Calculate usage of home directory
set usage [exec -- /bin/df -h ]
set usg8 [lindex $usage 8]
set usg9 [lindex $usage 9]
set usg10 [lindex $usage 10]
set usg11 [lindex $usage 11]

# * Calculate current system load
set loadavg     [exec -- /bin/cat /proc/loadavg]
set sysload(1)  [lindex $loadavg 0]
set sysload(5)  [lindex $loadavg 1]
set sysload(15) [lindex $loadavg 2]

# * Calculate Memory
set memory  [exec -- free -m]
set mem(t)  [lindex $memory 7]
set mem(u)  [lindex $memory 8]
set mem(f)  [lindex $memory 9]
set mem(c)  [lindex $memory 16]
set mem(s)  [lindex $memory 19]

# Versions
set ver  [exec -- dpkg -s rdcsetup]
set ver11 [lindex $ver 11]
set oiv [exec -- cat /usr/local/oivision/ver]
set oir [exec -- cat /usr/local/oiremote/update_ver]
set rdcsd [exec -- cat /boot/rdc-sd-ver]

# * display kernel version
set uname [exec -- /bin/uname -snrvm]
set unameoutput0 [lindex $uname 0]
set unameoutput1 [lindex $uname 1]
set unameoutput2 [lindex $uname 2]
set unameoutput3 [lindex $uname 3]


puts "   Oxford Instruments Remote Diagnostics Computer"
puts "   RDC SD Version..: $rdcsd"
puts "   RDC DEB Version.: $ver11"
puts "   OiVision Version: $oiv"
puts "   OiRemote Version: $oir"
puts "   System..........: $unameoutput0 $unameoutput1 $unameoutput2 $unameoutput3"
puts "   Last Login......: $ll(1) $ll(2) $ll(3) $ll(4) from $ll(5)"
puts "   Uptime..........: $up(days)days $up(hours)hours $up(mins)minutes $up(secs)seconds"
puts "   Load............: $sysload(1) (1minute) $sysload(5) (5minutes) $sysload(15) (15minutes)"
puts "   Memory MB.......: Total: $mem(t)  Used: $mem(u)  Free: $mem(f)  Cached: $mem(c)  Swap: $mem(s)"
puts "   Disk Usage......: Size: $usg8 Used: $usg9 Avail: $usg10 Use%: $usg11 "

if {[file exists /etc/changelog]&&[file readable /etc/changelog]} {
  puts " . .. More or less important system informations:\n"
  set fp [open /etc/changelog]
  while {-1!=[gets $fp line]} {
    puts "  ..) $line"
  }
  close $fp
  puts ""
}
